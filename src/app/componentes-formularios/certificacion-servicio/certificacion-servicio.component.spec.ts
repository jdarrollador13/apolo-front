import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificacionServicioComponent } from './certificacion-servicio.component';

describe('CertificacionServicioComponent', () => {
  let component: CertificacionServicioComponent;
  let fixture: ComponentFixture<CertificacionServicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificacionServicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificacionServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
