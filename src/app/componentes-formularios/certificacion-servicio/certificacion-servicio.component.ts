import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Integracion } from '../../integraciones/integracion.service';
import { AppGlobals } from 'src/app/app.global';
@Component({
  selector: 'app-certificacion-servicio',
  templateUrl: './certificacion-servicio.component.html',
  styleUrls: ['./certificacion-servicio.component.css'],
  providers: [AppGlobals, Integracion]
})
export class CertificacionServicioComponent implements OnInit {
	integracionhada:boolean = false
	resultNoOk:string|any
	msgCertificacionHADA:string|any
	certificacionHADA:boolean = false
	msgresultNoOk:boolean = false
	msgresultOk:boolean = false
	//seleccionPositiva:boolean = false
	URL:any;
	integracionProceso:any
	respuestaProcesoActual:any
	//=========== Componente Padre =================//
	@Input() public codComponentePasos:any
	@Input() public ListaPasos = [];
	@Input() public pasoActual:any;
	@Input() public ordenActivity:any
	@Input() public parametrosPoceso:any
	@Input() public dataFlujoOrden:any

  @Output() seleccionPositiva = new EventEmitter()

  constructor(
  	private global: AppGlobals,
		private integracionProces: Integracion,
	) { 
  	this.URL = this.global.url
	}

  ngOnInit() {
    setTimeout(() => {
      this.validarCategoria()
    },400)
  }
  async validarCategoria():Promise<void> {
    let resParametros:object|any
    if(this.dataFlujoOrden.XA_ACCESS_TECHNOLOGY == 'COBRE'){
      if(this.codComponentePasos.Sigla == "CERHADA"){
        //this.ProcesoActual = false 
        this.integracionhada = true
      }
    }
  }
  //VALIDA SI EJECUTA INTEGRACION TOA/-O-/HADA
  async validarEjecucionCertificacionH(value:string|any):Promise<void> {
    let respuestaIntg:object|any
    if(value == 'INTEGRACION_TOA'){
      respuestaIntg = await this.ejecutarProcesoSinEvento()
      console.log(respuestaIntg,'ii')
      if(respuestaIntg.llavePropiedad == 'NOOK'){
        this.integracionhada = false
        this.resultNoOk = 'No se evidencia en TOA el resultado ok de la certificación de servicio'
        this.msgCertificacionHADA = '¿ Deseas que se realice la certificación de servicio ahora ?'
        this.certificacionHADA = true
        this.msgresultNoOk = true
        setTimeout(() =>{ this.msgresultNoOk = false },12000)
      }
      if(respuestaIntg.llavePropiedad == 'OK'){
        this.resultNoOk = 'se evidencia en TOA el resultado ok de la certificación de servicio, continuar con la siguiente actividad.'
        this.msgresultOk = true
        this.integracionhada = false
        respuestaIntg.seleccionPositiva = false
        setTimeout(() =>{ 
          this.msgresultOk = false, 
          //this.seleccionPositiva = false 
          this.integracionhada = false
          this.seleccionPositiva.emit(respuestaIntg)
        },10000)
      }
    } else if(value == 'NO_INTEGRACION_HADA'){
      this.msgresultNoOk = true
      this.resultNoOk = 'Debes asegurar la certificación de servicio correcta para continuar en el flujo'
    }
  }
  //Ejecuta Integracion Sin evento con el BOTTON
  async ejecutarProcesoSinEvento ():Promise<any>{
    //this.loading = true;
    this.codComponentePasos = this.ListaPasos.find(x => x.Id_Paso == this.pasoActual);
    this.integracionProceso = { "parametros": {
          "ordenAtivity": this.ordenActivity,
          "paramtros": {
            "value": this.parametrosPoceso || ''
          },
        },
        "sigla": this.codComponentePasos.Sigla || ''
    };
    this.respuestaProcesoActual = await this.integracionProces.proces(this.integracionProceso, this.URL);
    return this.respuestaProcesoActual
  }

}
